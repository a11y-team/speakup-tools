version = 0.0
package = speakup-tools

prefix = /usr/local
bindir = $(prefix)/bin
sbindir = $(prefix)/sbin
datadir = $(prefix)/share
mandir = $(datadir)/man
pkgdatadir = $(datadir)/$(package)

CHMOD = chmod
INSTALL = install
SED = sed
